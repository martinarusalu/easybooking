<?php
require_once("Controller.php");

$putData = array(
		"time" => date("Y-m-d H:i:s"),
		"name" => "Peeter Rebane",
		"telephone" => "56668545",
		"email" => "asi@gmail.com",
		"people" => 8
);

$bookings = Controller::getBookings();

?>
<a href="book.php">Broneerima!</a>
<?php foreach($bookings as $booking) : ?>
<h1>Broneeringu aeg: <?php echo $booking["time"] ?></h1>
<h2>Nimi: <?php echo $booking["name"] ?></h2>
<p>E-mail: <?php echo $booking["email"] ?></p>
<p>Telefoni number: <?php echo $booking["telephone"] ?></p>
<p>Inimeste arv: <?php echo $booking["people"] ?></p>
<br>
<?php endforeach;?>