<?php
require_once("Model.php");

class Controller {
	public static function getBookings () {
		return Model::getBookings();
	}
	
	public static function putBooking ($data) {
		Model::putBooking($data);
	}
	
	public static function validateTime($time) {
		if (strtotime($time) > strtotime(date())) {
			return true;
		} else {
			return false;
		}
	}
	
}