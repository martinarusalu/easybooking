<?php
define("HOST","localhost");
define("USER","test");
define("PASS","t3st3r123");
define("DB","test");
define("TABLE", "marusalu_bookings");

class Model {
	private static function getDbCon () {
		$con = mysqli_connect(HOST, USER, PASS, DB);
		if ($con->connect_error) {
			die("Connection failed: " . $con->connect_error);
		} else {
			return $con;
		}
	}
	
	public static function putBooking ($data) {
		$con = self::getDbCon();
	
		$sql = "INSERT INTO ".TABLE." (time, name, telephone, email, people) VALUES (?, ?, ?, ?, ?);";
		$query = $con->prepare($sql);
		$query->bind_param('ssssi', $data["time"], $data["name"], $data["telephone"], $data["email"], $data["people"]);
		$query->execute();
	
		$result = $query->get_result();
		while ($row = $result->fetch_assoc()) {
		}
	
		$con->close();
	}
	
	public static function getBookings () {
		$con = self::getDbCon();
		$data = array();
	
		$sql = "SELECT time, name, telephone, email, people FROM ".TABLE.";";
		$query = $con->prepare($sql);
		$query->execute();
	
		$result = $query->get_result();
		while ($row = $result->fetch_assoc()) {
			$data[] = array (
					"time" => $row["time"],
					"name" => $row["name"],
					"telephone" => $row["telephone"],
					"email" => $row["email"],
					"people" => intval($row["people"])
			);
		}
	
		$con->close();
	
		return $data;
	}
	
}
